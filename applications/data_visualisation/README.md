DATA VISUALISATION
==================

This directory contains a series of practicals which focus
on using Python for data visualisation.

1. `bokeh` - package for creating interactive visualisations. This folder contains two files: a notebook that collates multiple examples from the `bokeh` documentation into a single notebook, and an example Python script that uses `bokeh` to create a simple GUI to run FSL BET.
2. `fsleyes_render.md` - learn to use FSLeyes in the command line to create a range of static and dynamic figures. This is not focused on Python per se.
3. `matplotlib.ipynb` - the most popular generic data visualisation package in Python.
4. `plotly.ipynb` - an alternative to `bokeh` for creating interactive plots.
5. `nilearn.ipynb` - package for doing machine learning in neuroimaging in Python. This tutorial focuses on using the visualisation tools in `nilearn`.
