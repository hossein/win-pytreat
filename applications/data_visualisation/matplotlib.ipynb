{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fa095385",
   "metadata": {},
   "source": [
    "# Matplotlib tutorial\n",
    "\n",
    "The main plotting library in python is `matplotlib`.  \n",
    "It provides a simple interface to just explore the data, \n",
    "while also having a lot of flexibility to create publication-worthy plots.\n",
    "In fact, the vast majority of python-produced plots in papers will be either produced\n",
    "directly using matplotlib or by one of the many plotting libraries built on top of\n",
    "matplotlib (such as [seaborn](https://seaborn.pydata.org/) or [nilearn](https://nilearn.github.io/)).\n",
    "\n",
    "Like everything in python, there is a lot of help available online (just google it or ask your local pythonista).\n",
    "A particularly useful resource for matplotlib is the [gallery](https://matplotlib.org/gallery/index.html).\n",
    "Here you can find a wide range of plots. \n",
    "Just find one that looks like what you want to do and click on it to see (and copy) the code used to generate the plot.\n",
    "\n",
    "## Contents\n",
    "- [Basic plotting commands](#basic-plotting-commands)\n",
    "  - [Line plots](#line)\n",
    "  - [Scatter plots](#scatter)\n",
    "  - [Histograms and bar plots](#histograms)\n",
    "  - [Adding error bars](#error)\n",
    "  - [Shading regions](#shade)\n",
    "  - [Displaying images](#image)\n",
    "  - [Adding lines, arrows, text](#annotations)\n",
    "- [Using the object-oriented interface](#OO)\n",
    "- [Multiple plots (i.e., subplots)](#subplots)\n",
    "  - [Adjusting plot layouts](#layout)\n",
    "  - [Advanced grid configurations (GridSpec)](#grid-spec)\n",
    "- [Styling your plot](#styling)\n",
    "  - [Setting title and labels](#labels)\n",
    "  - [Editing the x- and y-axis](#axis)\n",
    "- [FAQ](#faq)\n",
    "  - [Why am I getting two images?](#double-image)\n",
    "  - [I produced a plot in my python script, but it does not show up](#show)\n",
    "  - [Changing where the image appears: backends](#backends)\n",
    "\n",
    "<a class=\"anchor\" id=\"basic-plotting-commands\"></a>\n",
    "## Basic plotting commands\n",
    "Let's start with the basic imports:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41578cdc",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a9a5f55",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"line\"></a>\n",
    "### Line plots\n",
    "A basic lineplot can be made just by calling `plt.plot`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2531bb20",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot([1, 2, 3], [1.3, 4.2, 3.1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9ef51d5c",
   "metadata": {},
   "source": [
    "To adjust how the line is plotted, check the documentation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9a768ab3",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2e6a4d1",
   "metadata": {},
   "source": [
    "As you can see there are a lot of options.\n",
    "The ones you will probably use most often are:\n",
    "- `linestyle`: how the line is plotted (set to '' to omit the line)\n",
    "- `marker`: how the points are plotted (these are not plotted by default)\n",
    "- `color`: what color to use (defaults to cycling through a set of 7 colors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "85ed5f73",
   "metadata": {},
   "outputs": [],
   "source": [
    "theta = np.linspace(0, 2 * np.pi, 101)\n",
    "plt.plot(np.sin(theta), np.cos(theta))\n",
    "plt.plot([-0.3, 0.3], [0.3, 0.3], marker='o', linestyle='', markersize=20)\n",
    "plt.plot(0, -0.1, marker='s', color='black')\n",
    "x = np.linspace(-0.5, 0.5, 5)\n",
    "plt.plot(x, x ** 2 - 0.5, linestyle='--', marker='+', color='red')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a359e01a",
   "metadata": {},
   "source": [
    "Because these keywords are so common, you can actually set one or more of them by passing in a string as the third argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e69e842",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.linspace(0, 1, 11)\n",
    "plt.plot(x, x)\n",
    "plt.plot(x, x ** 2, '--') # sets the linestyle to dashed\n",
    "plt.plot(x, x ** 3, 's')  # sets the marker to square (and turns off the line)\n",
    "plt.plot(x, x ** 4, '^y:') # sets the marker to triangles (i.e., '^'), linestyle to dotted (i.e., ':'), and the color to yellow (i.e., 'y')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "891a9f9e",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"scatter\"></a>\n",
    "### Scatter plots\n",
    "The main extra feature of `plt.scatter` over `plt.plot` is that you can vary the color and size of the points based on some other variable array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8cb13b7e",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.random.rand(30)\n",
    "y = np.random.rand(30)\n",
    "plt.scatter(x, y, x * 30, y)\n",
    "plt.colorbar()  # adds a colorbar"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df311f5c",
   "metadata": {},
   "source": [
    "The third argument is the variable determining the size, while the fourth argument is the variable setting the color.\n",
    "<a class=\"anchor\" id=\"histograms\"></a>\n",
    "### Histograms and bar plots\n",
    "For a simple histogram you can do this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9bb4e76",
   "metadata": {},
   "outputs": [],
   "source": [
    "r = np.random.rand(1000)\n",
    "n,bins,_ = plt.hist((r-0.5)**2, bins=30)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "141bf7e8",
   "metadata": {},
   "source": [
    "where it also returns the number of elements in each bin, as `n`, and\n",
    "the bin centres, as `bins`.\n",
    "\n",
    "> The `_` in the third part on the left\n",
    "> hand side is a shorthand for just throwing away the corresponding part\n",
    "> of the return structure.\n",
    "\n",
    "\n",
    "There is also a call for doing bar plots:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "951bd53e",
   "metadata": {},
   "outputs": [],
   "source": [
    "samp1 = r[0:10]\n",
    "samp2 = r[10:20]\n",
    "bwidth = 0.3\n",
    "xcoord = np.arange(10)\n",
    "plt.bar(xcoord-bwidth, samp1, width=bwidth, color='red', label='Sample 1')\n",
    "plt.bar(xcoord, samp2, width=bwidth, color='blue', label='Sample 2')\n",
    "plt.legend(loc='upper left')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2ae38282",
   "metadata": {},
   "source": [
    "> If you want more advanced distribution plots beyond a simple histogram, have a look at the seaborn [gallery](https://seaborn.pydata.org/examples/index.html) for (too?) many options.\n",
    "\n",
    "<a class=\"anchor\" id=\"error\"></a>\n",
    "### Adding error bars\n",
    "If your data is not completely perfect and has for some obscure reason some uncertainty associated with it, \n",
    "you can plot these using `plt.error`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3f440fd0",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.arange(5)\n",
    "y1 = [0.3, 0.5, 0.7, 0.1, 0.3]\n",
    "yerr = [0.12, 0.28, 0.1, 0.25, 0.6]\n",
    "xerr = 0.3\n",
    "plt.errorbar(x, y1, yerr, xerr, marker='s', linestyle='')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1405cf82",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"shade\"></a>\n",
    "### Shading regions\n",
    "An area below a plot can be shaded using `plt.fill`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c0f12a0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.linspace(0, 2, 100)\n",
    "plt.fill(x, np.sin(x * np.pi))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "71d7bc82",
   "metadata": {},
   "source": [
    "This can be nicely combined with a polar projection, to create 2D orientation distribution functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e337ced8",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplot(projection='polar')\n",
    "theta = np.linspace(0, 2 * np.pi, 100)\n",
    "plt.fill(theta, np.exp(-2 * np.cos(theta) ** 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12c4eee6",
   "metadata": {},
   "source": [
    "The area between two lines can be shaded using `fill_between`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54c6b838",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.linspace(0, 10, 1000)\n",
    "y = 5 * np.sin(5 * x) + x - 0.1 * x ** 2\n",
    "yl = x - 0.1 * x ** 2 - 5\n",
    "yu = yl + 10\n",
    "plt.plot(x, y, 'r')\n",
    "plt.fill_between(x, yl, yu)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a1d3815",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"image\"></a>\n",
    "### Displaying images\n",
    "The main command for displaying images is `plt.imshow` (use `plt.pcolor` for cases where you do not have a regular grid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ed051029",
   "metadata": {},
   "outputs": [],
   "source": [
    "import nibabel as nib\n",
    "import os.path as op\n",
    "nim = nib.load(op.expandvars('${FSLDIR}/data/standard/MNI152_T1_1mm.nii.gz'), mmap=False)\n",
    "imdat = nim.get_data().astype(float)\n",
    "imslc = imdat[:,:,70]\n",
    "plt.imshow(imslc, cmap=plt.cm.gray)\n",
    "plt.colorbar()\n",
    "plt.axis('off')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "156b0628",
   "metadata": {},
   "source": [
    "Note that matplotlib will use the **voxel data orientation**, and that\n",
    "configuring the plot orientation is **your responsibility**. To rotate a\n",
    "slice, simply transpose the data (`.T`). To invert the data along along an\n",
    "axis, you don't need to modify the data - simply swap the axis limits around:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a65cf0d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(imslc.T, cmap=plt.cm.gray)\n",
    "plt.xlim(reversed(plt.xlim()))\n",
    "plt.ylim(reversed(plt.ylim()))\n",
    "plt.colorbar()\n",
    "plt.axis('off')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c8a01a8",
   "metadata": {},
   "source": [
    "> It is easier to produce informative brain images using nilearn or fsleyes\n",
    "<a class=\"anchor\" id=\"annotations\"></a>\n",
    "### Adding lines, arrows, and text\n",
    "Adding horizontal/vertical lines, arrows, and text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3f9f4fad",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.axhline(-1)  # horizontal line\n",
    "plt.axvline(1) # vertical line\n",
    "plt.arrow(0.2, -0.2, 0.2, -0.8, length_includes_head=True, width=0.01)\n",
    "plt.text(0.5, 0.5, 'middle of the plot', transform=plt.gca().transAxes, ha='center', va='center')\n",
    "plt.annotate(\"line crossing\", (1, -1), (0.8, -0.8), arrowprops={})  # adds both text and arrow; need to set the arrowprops keyword for the arrow to be plotted"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2fb44b4",
   "metadata": {},
   "source": [
    "By default the locations of the arrows and text will be in data coordinates (i.e., whatever is on the axes),\n",
    "however you can change that. For example to find the middle of the plot in the last example we use\n",
    "axes coordinates, which are always (0, 0) in the lower left and (1, 1) in the upper right.\n",
    "See the matplotlib [transformations tutorial](https://matplotlib.org/stable/tutorials/advanced/transforms_tutorial.html)\n",
    "for more detail.\n",
    "\n",
    "<a class=\"anchor\" id=\"OO\"></a>\n",
    "## Using the object-oriented interface\n",
    "In the examples above we simply added multiple lines/points/bars/images \n",
    "(collectively called [artists](https://matplotlib.org/stable/tutorials/intermediate/artists.html) in matplotlib) to a single plot.\n",
    "To prettify this plots, we first need to know what all the features are called:\n",
    "\n",
    "![anatomy of a plot](https://matplotlib.org/stable/_images/anatomy.png)\n",
    "\n",
    "Using the terms in this plot let's see what our first command of `plt.plot([1, 2, 3], [1.3, 4.2, 3.1])`\n",
    "actually does:\n",
    "\n",
    "1. First it creates a figure and makes this the active figure. Being the active figure means that any subsequent commands will affect figure. You can find the active figure at any point by calling `plt.gcf()`.\n",
    "2. Then it creates an Axes or Subplot in the figure and makes this the active axes. Any subsequent commands will reuse this active axes. You can find the active axes at any point by calling `plt.gca()`.\n",
    "3. Finally it creates a Line2D artist containing the x-coordinates `[1, 2, 3]` and `[1.3, 4.2, 3.1]` ands adds this to the active axes.\n",
    "4. At some later time, when actually creating the plot, matplotlib will also automatically determine for you a default range for the x-axis and y-axis and where the ticks should be.\n",
    "\n",
    "This concept of an \"active\" figure and \"active\" axes can be very helpful with a single plot, it can quickly get very confusing when you have multiple sub-plots within a figure or even multiple figures.\n",
    "In that case we want to be more explicit about what sub-plot we want to add the artist to.\n",
    "We can do this by switching from the \"procedural\" interface used above to the \"object-oriented\" interface.\n",
    "The commands are very similar, we just have to do a little more setup.\n",
    "For example, the equivalent of `plt.plot([1, 2, 3], [1.3, 4.2, 3.1])` is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43229971",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.add_subplot()\n",
    "ax.plot([1, 2, 3], [1.3, 4.2, 3.1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d4bee33",
   "metadata": {},
   "source": [
    "Note that here we explicitly create the figure and add a single sub-plot to the figure.\n",
    "We then call the `plot` function explicitly on this figure.\n",
    "The \"Axes\" object has all of the same plotting command as we used above,\n",
    "although the commands to adjust the properties of things like the title, x-axis, and y-axis are slighly different.\n",
    "\n",
    "`plt.getp` gives a helpful summary of the properties of a matplotlib object (and what you might change):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2cc5123a",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.getp(ax)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37251f4a",
   "metadata": {},
   "source": [
    "When going through this list carefully you might have spotted that the plotted line is stored in the `lines` property.\n",
    "Let's have a look at this line in more detail"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db290a0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.getp(ax.lines[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae053e0c",
   "metadata": {},
   "source": [
    "This shows us all the properties stored about this line,\n",
    "including its coordinates in many different formats \n",
    "(`data`, `path`, `xdata`, `ydata`, or `xydata`),\n",
    "the line style and width (`linestyle`, `linewidth`), `color`, etc.\n",
    "\n",
    "<a class=\"anchor\" id=\"subplots\"></a>\n",
    "## Multiple plots (i.e., subplots)\n",
    "As stated one of the strengths of the object-oriented interface is that it is easier to work with multiple plots. \n",
    "While we could do this in the procedural interface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8bd710d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplot(221)\n",
    "plt.title(\"Upper left\")\n",
    "plt.subplot(222)\n",
    "plt.title(\"Upper right\")\n",
    "plt.subplot(223)\n",
    "plt.title(\"Lower left\")\n",
    "plt.subplot(224)\n",
    "plt.title(\"Lower right\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28b82718",
   "metadata": {},
   "source": [
    "For such a simple example, this works fine. But for longer examples you would find yourself constantly looking back through the\n",
    "code to figure out which of the subplots this specific `plt.title` command is affecting.\n",
    "\n",
    "The recommended way to this instead is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "89a20086",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(nrows=2, ncols=2)\n",
    "axes[0, 0].set_title(\"Upper left\")\n",
    "axes[0, 1].set_title(\"Upper right\")\n",
    "axes[1, 0].set_title(\"Lower left\")\n",
    "axes[1, 1].set_title(\"Lower right\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "852c2d46",
   "metadata": {},
   "source": [
    "Here we use `plt.subplots`, which creates both a new figure for us and a grid of sub-plots. \n",
    "The returned `axes` object is in this case a 2x2 array of `Axes` objects, to which we set the title using the normal numpy indexing.\n",
    "\n",
    "> Seaborn is great for creating grids of closely related plots. Before you spent a lot of time implementing your own have a look if seaborn already has what you want on their [gallery](https://seaborn.pydata.org/examples/index.html)\n",
    "<a class=\"anchor\" id=\"layout\"></a>\n",
    "### Adjusting plot layout\n",
    "The default layout of sub-plots often leads to overlap between the labels/titles of the various subplots (as above) or to excessive amounts of whitespace in between. We can often fix this by just adding `fig.tight_layout` (or `plt.tight_layout`) after making the plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c14ec50",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(nrows=2, ncols=2)\n",
    "axes[0, 0].set_title(\"Upper left\")\n",
    "axes[0, 1].set_title(\"Upper right\")\n",
    "axes[1, 0].set_title(\"Lower left\")\n",
    "axes[1, 1].set_title(\"Lower right\")\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "338c7239",
   "metadata": {},
   "source": [
    "Uncomment `fig.tight_layout` and see how it adjusts the spacings between the plots automatically to reduce the whitespace.\n",
    "If you want more explicit control, you can use `fig.subplots_adjust` (or `plt.subplots_adjust` to do this for the active figure).\n",
    "For example, we can remove any whitespace between the plots using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5df7361f",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(1)\n",
    "fig, axes = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=True)\n",
    "for ax in axes.flat:\n",
    "    offset = np.random.rand(2) * 5\n",
    "    ax.scatter(np.random.randn(10) + offset[0], np.random.randn(10) + offset[1])\n",
    "fig.suptitle(\"group of plots, sharing x- and y-axes\")\n",
    "fig.subplots_adjust(wspace=0, hspace=0, top=0.9)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff58c930",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"grid-spec\"></a>\n",
    "### Advanced grid configurations (GridSpec)\n",
    "You can create more advanced grid layouts using [GridSpec](https://matplotlib.org/stable/tutorials/intermediate/gridspec.html).\n",
    "An example taken from that website is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c1651d0c",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(constrained_layout=True)\n",
    "gs = fig.add_gridspec(3, 3)\n",
    "f3_ax1 = fig.add_subplot(gs[0, :])\n",
    "f3_ax1.set_title('gs[0, :]')\n",
    "f3_ax2 = fig.add_subplot(gs[1, :-1])\n",
    "f3_ax2.set_title('gs[1, :-1]')\n",
    "f3_ax3 = fig.add_subplot(gs[1:, -1])\n",
    "f3_ax3.set_title('gs[1:, -1]')\n",
    "f3_ax4 = fig.add_subplot(gs[-1, 0])\n",
    "f3_ax4.set_title('gs[-1, 0]')\n",
    "f3_ax5 = fig.add_subplot(gs[-1, -2])\n",
    "f3_ax5.set_title('gs[-1, -2]')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5676c42d",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"styling\"></a>\n",
    "## Styling your plot\n",
    "<a class=\"anchor\" id=\"labels\"></a>\n",
    "### Setting title and labels\n",
    "You can edit a large number of plot properties by using the `Axes.set_*` interface.\n",
    "We have already seen several examples of this above, but here is one more:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b6841514",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots()\n",
    "axes.plot([1, 2, 3], [2.3, 4.1, 0.8])\n",
    "axes.set_xlabel('xlabel')\n",
    "axes.set_ylabel('ylabel')\n",
    "axes.set_title('title')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c27500eb",
   "metadata": {},
   "source": [
    "You can also set any of these properties by calling `Axes.set` directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4aa8461b",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots()\n",
    "axes.plot([1, 2, 3], [2.3, 4.1, 0.8])\n",
    "axes.set(\n",
    "    xlabel='xlabel',\n",
    "    ylabel='ylabel',\n",
    "    title='title',\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e69e0f4b",
   "metadata": {},
   "source": [
    "> To match the matlab API and save some typing the equivalent commands in the procedural interface do not have the `set_` preset. So, they are `plt.xlabel`, `plt.ylabel`, `plt.title`. This is also true for many of the `set_` commands we will see below.\n",
    "\n",
    "You can edit the font of the text when setting the label or after the fact using the object-oriented interface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9958b2e",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots()\n",
    "axes.plot([1, 2, 3], [2.3, 4.1, 0.8])\n",
    "axes.set_xlabel(\"xlabel\", color='red')\n",
    "axes.set_ylabel(\"ylabel\")\n",
    "axes.get_yaxis().get_label().set_fontsize('larger')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "111da8e1",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"axis\"></a>\n",
    "### Editing the x- and y-axis\n",
    "We can change many of the properties of the x- and y-axis by using `set_` commands.\n",
    "\n",
    "- The range shown on an axis can be set using `ax.set_xlim` (or `plt.xlim`)\n",
    "- You can switch to a logarithmic (or other) axis using `ax.set_xscale('log')`\n",
    "- The location of the ticks can be set using `ax.set_xticks` (or `plt.xticks`)\n",
    "- The text shown for the ticks can be set using `ax.set_xticklabels` (or as a second argument to `plt.xticks`)\n",
    "- The style of the ticks can be adjusted by looping through the ticks (obtained through `ax.get_xticks` or calling `plt.xticks` without arguments).\n",
    "\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e402140",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots()\n",
    "axes.errorbar([0, 1, 2], [0.8, 0.4, -0.2], 0.1, linestyle='-', marker='s')\n",
    "axes.set_xticks((0, 1, 2))\n",
    "axes.set_xticklabels(('start', 'middle', 'end'))\n",
    "for tick in axes.get_xticklabels():\n",
    "    tick.set(\n",
    "        rotation=45,\n",
    "        size='larger'\n",
    "    )\n",
    "axes.set_xlabel(\"Progression through practical\")\n",
    "axes.set_yticks((0, 0.5, 1))\n",
    "axes.set_yticklabels(('0', '50%', '100%'))\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9bd34f1c",
   "metadata": {},
   "source": [
    "As illustrated earlier, we can get a more complete list of the things we could change about the x-axis by looking at its properties:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db2b0e6e",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.getp(axes.get_xaxis())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48b79b04",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"faq\"></a>\n",
    "## FAQ\n",
    "<a class=\"anchor\" id=\"double-image\"></a>\n",
    "### Why am I getting two images?\n",
    "Any figure you produce in the notebook will be shown by default once a cell successfully finishes (i.e., without error).\n",
    "If the code in a notebook cell crashes after creating the figure, this figure will still be in memory.\n",
    "It will be shown after another cell successfully finishes.\n",
    "You can remove this additional plot simply by rerunning the cell, after which you should only see the plot produced by the cell in question.\n",
    "\n",
    "<a class=\"anchor\" id=\"show\"></a>\n",
    "### I produced a plot in my python script, but it does not show up?\n",
    "Add `plt.show()` to the end of your script (or save the figure to a file using `plt.savefig` or `fig.savefig`).\n",
    "`plt.show` will show the image to you and will block the script to allow you to take in and adjust the figure before saving or discarding it.\n",
    "\n",
    "<a class=\"anchor\" id=\"backends\"></a>\n",
    "### Changing where the image appears: backends\n",
    "Matplotlib works across a wide range of environments: Linux, Mac OS, Windows, in the browser, and more. \n",
    "The exact detail of how to show you your plot will be different across all of these environments.\n",
    "This procedure used to translate your `Figure`/`Axes` objects into an actual visualisation is called the backend.\n",
    "\n",
    "In this notebook we were using the `inline` backend, which is the default when running in a notebook.\n",
    "While very robust, this backend has the disadvantage that it only produces static plots.\n",
    "We could have had interactive plots if only we had changed backends to `nbagg`.\n",
    "You can change backends in the IPython terminal/notebook using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e36ee821",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib nbagg"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68b0aac8",
   "metadata": {},
   "source": [
    "> If you are using Jupyterlab (new version of the jupyter notebook) the `nbagg` backend will not work. Instead you will have to install `ipympl` and then use the `widgets` backend to get an interactive backend (this also works in the old notebooks).\n",
    "\n",
    "In python scripts, this will give you a syntax error and you should instead use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b81eb924",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "matplotlib.use(\"MacOSX\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14663014",
   "metadata": {},
   "source": [
    "Usually, the default backend will be fine, so you will not have to set it. \n",
    "Note that setting it explicitly will make your script less portable."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
