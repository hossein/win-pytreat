# Matplotlib tutorial

The main plotting library in python is `matplotlib`.  
It provides a simple interface to just explore the data, 
while also having a lot of flexibility to create publication-worthy plots.
In fact, the vast majority of python-produced plots in papers will be either produced
directly using matplotlib or by one of the many plotting libraries built on top of
matplotlib (such as [seaborn](https://seaborn.pydata.org/) or [nilearn](https://nilearn.github.io/)).

Like everything in python, there is a lot of help available online (just google it or ask your local pythonista).
A particularly useful resource for matplotlib is the [gallery](https://matplotlib.org/gallery/index.html).
Here you can find a wide range of plots. 
Just find one that looks like what you want to do and click on it to see (and copy) the code used to generate the plot.

## Contents
- [Basic plotting commands](#basic-plotting-commands)
  - [Line plots](#line)
  - [Scatter plots](#scatter)
  - [Histograms and bar plots](#histograms)
  - [Adding error bars](#error)
  - [Shading regions](#shade)
  - [Displaying images](#image)
  - [Adding lines, arrows, text](#annotations)
- [Using the object-oriented interface](#OO)
- [Multiple plots (i.e., subplots)](#subplots)
  - [Adjusting plot layouts](#layout)
  - [Advanced grid configurations (GridSpec)](#grid-spec)
- [Styling your plot](#styling)
  - [Setting title and labels](#labels)
  - [Editing the x- and y-axis](#axis)
- [FAQ](#faq)
  - [Why am I getting two images?](#double-image)
  - [I produced a plot in my python script, but it does not show up](#show)
  - [Changing where the image appears: backends](#backends)

<a class="anchor" id="basic-plotting-commands"></a>
## Basic plotting commands
Let's start with the basic imports:
```
import matplotlib.pyplot as plt
import numpy as np
```
<a class="anchor" id="line"></a>
### Line plots
A basic lineplot can be made just by calling `plt.plot`:
```
plt.plot([1, 2, 3], [1.3, 4.2, 3.1])
```

To adjust how the line is plotted, check the documentation:
```
plt.plot?
```

As you can see there are a lot of options.
The ones you will probably use most often are:
- `linestyle`: how the line is plotted (set to '' to omit the line)
- `marker`: how the points are plotted (these are not plotted by default)
- `color`: what color to use (defaults to cycling through a set of 7 colors)
```
theta = np.linspace(0, 2 * np.pi, 101)
plt.plot(np.sin(theta), np.cos(theta))
plt.plot([-0.3, 0.3], [0.3, 0.3], marker='o', linestyle='', markersize=20)
plt.plot(0, -0.1, marker='s', color='black')
x = np.linspace(-0.5, 0.5, 5)
plt.plot(x, x ** 2 - 0.5, linestyle='--', marker='+', color='red')
```
Because these keywords are so common, you can actually set one or more of them by passing in a string as the third argument.
```
x = np.linspace(0, 1, 11)
plt.plot(x, x)
plt.plot(x, x ** 2, '--') # sets the linestyle to dashed
plt.plot(x, x ** 3, 's')  # sets the marker to square (and turns off the line)
plt.plot(x, x ** 4, '^y:') # sets the marker to triangles (i.e., '^'), linestyle to dotted (i.e., ':'), and the color to yellow (i.e., 'y')
```
<a class="anchor" id="scatter"></a>
### Scatter plots
The main extra feature of `plt.scatter` over `plt.plot` is that you can vary the color and size of the points based on some other variable array:
```
x = np.random.rand(30)
y = np.random.rand(30)
plt.scatter(x, y, x * 30, y)
plt.colorbar()  # adds a colorbar
```
The third argument is the variable determining the size, while the fourth argument is the variable setting the color.
<a class="anchor" id="histograms"></a>
### Histograms and bar plots
For a simple histogram you can do this:
```
r = np.random.rand(1000)
n,bins,_ = plt.hist((r-0.5)**2, bins=30)
```
where it also returns the number of elements in each bin, as `n`, and
the bin centres, as `bins`.

> The `_` in the third part on the left
> hand side is a shorthand for just throwing away the corresponding part
> of the return structure.


There is also a call for doing bar plots:
```
samp1 = r[0:10]
samp2 = r[10:20]
bwidth = 0.3
xcoord = np.arange(10)
plt.bar(xcoord-bwidth, samp1, width=bwidth, color='red', label='Sample 1')
plt.bar(xcoord, samp2, width=bwidth, color='blue', label='Sample 2')
plt.legend(loc='upper left')
```

> If you want more advanced distribution plots beyond a simple histogram, have a look at the seaborn [gallery](https://seaborn.pydata.org/examples/index.html) for (too?) many options.

<a class="anchor" id="error"></a>
### Adding error bars
If your data is not completely perfect and has for some obscure reason some uncertainty associated with it, 
you can plot these using `plt.error`:
```
x = np.arange(5)
y1 = [0.3, 0.5, 0.7, 0.1, 0.3]
yerr = [0.12, 0.28, 0.1, 0.25, 0.6]
xerr = 0.3
plt.errorbar(x, y1, yerr, xerr, marker='s', linestyle='')
```
<a class="anchor" id="shade"></a>
### Shading regions
An area below a plot can be shaded using `plt.fill`
```
x = np.linspace(0, 2, 100)
plt.fill(x, np.sin(x * np.pi))
```

This can be nicely combined with a polar projection, to create 2D orientation distribution functions:
```
plt.subplot(projection='polar')
theta = np.linspace(0, 2 * np.pi, 100)
plt.fill(theta, np.exp(-2 * np.cos(theta) ** 2))
```

The area between two lines can be shaded using `fill_between`:
```
x = np.linspace(0, 10, 1000)
y = 5 * np.sin(5 * x) + x - 0.1 * x ** 2
yl = x - 0.1 * x ** 2 - 5
yu = yl + 10
plt.plot(x, y, 'r')
plt.fill_between(x, yl, yu)
```

<a class="anchor" id="image"></a>
### Displaying images
The main command for displaying images is `plt.imshow` (use `plt.pcolor` for cases where you do not have a regular grid)

```
import nibabel as nib
import os.path as op
nim = nib.load(op.expandvars('${FSLDIR}/data/standard/MNI152_T1_1mm.nii.gz'), mmap=False)
imdat = nim.get_data().astype(float)
imslc = imdat[:,:,70]
plt.imshow(imslc, cmap=plt.cm.gray)
plt.colorbar()
plt.axis('off')
```

Note that matplotlib will use the **voxel data orientation**, and that
configuring the plot orientation is **your responsibility**. To rotate a
slice, simply transpose the data (`.T`). To invert the data along along an
axis, you don't need to modify the data - simply swap the axis limits around:


```
plt.imshow(imslc.T, cmap=plt.cm.gray)
plt.xlim(reversed(plt.xlim()))
plt.ylim(reversed(plt.ylim()))
plt.colorbar()
plt.axis('off')
```

> It is easier to produce informative brain images using nilearn or fsleyes
<a class="anchor" id="annotations"></a>
### Adding lines, arrows, and text
Adding horizontal/vertical lines, arrows, and text:
```
plt.axhline(-1)  # horizontal line
plt.axvline(1) # vertical line
plt.arrow(0.2, -0.2, 0.2, -0.8, length_includes_head=True, width=0.01)
plt.text(0.5, 0.5, 'middle of the plot', transform=plt.gca().transAxes, ha='center', va='center')
plt.annotate("line crossing", (1, -1), (0.8, -0.8), arrowprops={})  # adds both text and arrow; need to set the arrowprops keyword for the arrow to be plotted
```
By default the locations of the arrows and text will be in data coordinates (i.e., whatever is on the axes),
however you can change that. For example to find the middle of the plot in the last example we use
axes coordinates, which are always (0, 0) in the lower left and (1, 1) in the upper right.
See the matplotlib [transformations tutorial](https://matplotlib.org/stable/tutorials/advanced/transforms_tutorial.html)
for more detail.

<a class="anchor" id="OO"></a>
## Using the object-oriented interface
In the examples above we simply added multiple lines/points/bars/images 
(collectively called [artists](https://matplotlib.org/stable/tutorials/intermediate/artists.html) in matplotlib) to a single plot.
To prettify this plots, we first need to know what all the features are called:

![anatomy of a plot](https://matplotlib.org/stable/_images/anatomy.png)

Using the terms in this plot let's see what our first command of `plt.plot([1, 2, 3], [1.3, 4.2, 3.1])`
actually does:

1. First it creates a figure and makes this the active figure. Being the active figure means that any subsequent commands will affect figure. You can find the active figure at any point by calling `plt.gcf()`.
2. Then it creates an Axes or Subplot in the figure and makes this the active axes. Any subsequent commands will reuse this active axes. You can find the active axes at any point by calling `plt.gca()`.
3. Finally it creates a Line2D artist containing the x-coordinates `[1, 2, 3]` and `[1.3, 4.2, 3.1]` ands adds this to the active axes.
4. At some later time, when actually creating the plot, matplotlib will also automatically determine for you a default range for the x-axis and y-axis and where the ticks should be.

This concept of an "active" figure and "active" axes can be very helpful with a single plot, it can quickly get very confusing when you have multiple sub-plots within a figure or even multiple figures.
In that case we want to be more explicit about what sub-plot we want to add the artist to.
We can do this by switching from the "procedural" interface used above to the "object-oriented" interface.
The commands are very similar, we just have to do a little more setup.
For example, the equivalent of `plt.plot([1, 2, 3], [1.3, 4.2, 3.1])` is:
```
fig = plt.figure()
ax = fig.add_subplot()
ax.plot([1, 2, 3], [1.3, 4.2, 3.1])
```
Note that here we explicitly create the figure and add a single sub-plot to the figure.
We then call the `plot` function explicitly on this figure.
The "Axes" object has all of the same plotting command as we used above,
although the commands to adjust the properties of things like the title, x-axis, and y-axis are slighly different.

`plt.getp` gives a helpful summary of the properties of a matplotlib object (and what you might change):
```
plt.getp(ax)
```
When going through this list carefully you might have spotted that the plotted line is stored in the `lines` property.
Let's have a look at this line in more detail
```
plt.getp(ax.lines[0])
```
This shows us all the properties stored about this line,
including its coordinates in many different formats 
(`data`, `path`, `xdata`, `ydata`, or `xydata`),
the line style and width (`linestyle`, `linewidth`), `color`, etc.

<a class="anchor" id="subplots"></a>
## Multiple plots (i.e., subplots)
As stated one of the strengths of the object-oriented interface is that it is easier to work with multiple plots. 
While we could do this in the procedural interface:
```
plt.subplot(221)
plt.title("Upper left")
plt.subplot(222)
plt.title("Upper right")
plt.subplot(223)
plt.title("Lower left")
plt.subplot(224)
plt.title("Lower right")
```
For such a simple example, this works fine. But for longer examples you would find yourself constantly looking back through the
code to figure out which of the subplots this specific `plt.title` command is affecting.

The recommended way to this instead is:
```
fig, axes = plt.subplots(nrows=2, ncols=2)
axes[0, 0].set_title("Upper left")
axes[0, 1].set_title("Upper right")
axes[1, 0].set_title("Lower left")
axes[1, 1].set_title("Lower right")
```
Here we use `plt.subplots`, which creates both a new figure for us and a grid of sub-plots. 
The returned `axes` object is in this case a 2x2 array of `Axes` objects, to which we set the title using the normal numpy indexing.

> Seaborn is great for creating grids of closely related plots. Before you spent a lot of time implementing your own have a look if seaborn already has what you want on their [gallery](https://seaborn.pydata.org/examples/index.html)
<a class="anchor" id="layout"></a>
### Adjusting plot layout
The default layout of sub-plots often leads to overlap between the labels/titles of the various subplots (as above) or to excessive amounts of whitespace in between. We can often fix this by just adding `fig.tight_layout` (or `plt.tight_layout`) after making the plot:
```
fig, axes = plt.subplots(nrows=2, ncols=2)
axes[0, 0].set_title("Upper left")
axes[0, 1].set_title("Upper right")
axes[1, 0].set_title("Lower left")
axes[1, 1].set_title("Lower right")
fig.tight_layout()
```
Uncomment `fig.tight_layout` and see how it adjusts the spacings between the plots automatically to reduce the whitespace.
If you want more explicit control, you can use `fig.subplots_adjust` (or `plt.subplots_adjust` to do this for the active figure).
For example, we can remove any whitespace between the plots using:
```
np.random.seed(1)
fig, axes = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=True)
for ax in axes.flat:
    offset = np.random.rand(2) * 5
    ax.scatter(np.random.randn(10) + offset[0], np.random.randn(10) + offset[1])
fig.suptitle("group of plots, sharing x- and y-axes")
fig.subplots_adjust(wspace=0, hspace=0, top=0.9)
```

<a class="anchor" id="grid-spec"></a>
### Advanced grid configurations (GridSpec)
You can create more advanced grid layouts using [GridSpec](https://matplotlib.org/stable/tutorials/intermediate/gridspec.html).
An example taken from that website is:
```
fig = plt.figure(constrained_layout=True)
gs = fig.add_gridspec(3, 3)
f3_ax1 = fig.add_subplot(gs[0, :])
f3_ax1.set_title('gs[0, :]')
f3_ax2 = fig.add_subplot(gs[1, :-1])
f3_ax2.set_title('gs[1, :-1]')
f3_ax3 = fig.add_subplot(gs[1:, -1])
f3_ax3.set_title('gs[1:, -1]')
f3_ax4 = fig.add_subplot(gs[-1, 0])
f3_ax4.set_title('gs[-1, 0]')
f3_ax5 = fig.add_subplot(gs[-1, -2])
f3_ax5.set_title('gs[-1, -2]')
```

<a class="anchor" id="styling"></a>
## Styling your plot
<a class="anchor" id="labels"></a>
### Setting title and labels
You can edit a large number of plot properties by using the `Axes.set_*` interface.
We have already seen several examples of this above, but here is one more:
```
fig, axes = plt.subplots()
axes.plot([1, 2, 3], [2.3, 4.1, 0.8])
axes.set_xlabel('xlabel')
axes.set_ylabel('ylabel')
axes.set_title('title')
```
You can also set any of these properties by calling `Axes.set` directly:
```
fig, axes = plt.subplots()
axes.plot([1, 2, 3], [2.3, 4.1, 0.8])
axes.set(
    xlabel='xlabel',
    ylabel='ylabel',
    title='title',
)
```

> To match the matlab API and save some typing the equivalent commands in the procedural interface do not have the `set_` preset. So, they are `plt.xlabel`, `plt.ylabel`, `plt.title`. This is also true for many of the `set_` commands we will see below.

You can edit the font of the text when setting the label or after the fact using the object-oriented interface:
```
fig, axes = plt.subplots()
axes.plot([1, 2, 3], [2.3, 4.1, 0.8])
axes.set_xlabel("xlabel", color='red')
axes.set_ylabel("ylabel")
axes.get_yaxis().get_label().set_fontsize('larger')
```

<a class="anchor" id="axis"></a>
### Editing the x- and y-axis
We can change many of the properties of the x- and y-axis by using `set_` commands.

- The range shown on an axis can be set using `ax.set_xlim` (or `plt.xlim`)
- You can switch to a logarithmic (or other) axis using `ax.set_xscale('log')`
- The location of the ticks can be set using `ax.set_xticks` (or `plt.xticks`)
- The text shown for the ticks can be set using `ax.set_xticklabels` (or as a second argument to `plt.xticks`)
- The style of the ticks can be adjusted by looping through the ticks (obtained through `ax.get_xticks` or calling `plt.xticks` without arguments).

For example:

```
fig, axes = plt.subplots()
axes.errorbar([0, 1, 2], [0.8, 0.4, -0.2], 0.1, linestyle='-', marker='s')
axes.set_xticks((0, 1, 2))
axes.set_xticklabels(('start', 'middle', 'end'))
for tick in axes.get_xticklabels():
    tick.set(
        rotation=45,
        size='larger'
    )
axes.set_xlabel("Progression through practical")
axes.set_yticks((0, 0.5, 1))
axes.set_yticklabels(('0', '50%', '100%'))
fig.tight_layout()
```

As illustrated earlier, we can get a more complete list of the things we could change about the x-axis by looking at its properties:
```
plt.getp(axes.get_xaxis())
```

<a class="anchor" id="faq"></a>
## FAQ
<a class="anchor" id="double-image"></a>
### Why am I getting two images?
Any figure you produce in the notebook will be shown by default once a cell successfully finishes (i.e., without error).
If the code in a notebook cell crashes after creating the figure, this figure will still be in memory.
It will be shown after another cell successfully finishes.
You can remove this additional plot simply by rerunning the cell, after which you should only see the plot produced by the cell in question.

<a class="anchor" id="show"></a>
### I produced a plot in my python script, but it does not show up?
Add `plt.show()` to the end of your script (or save the figure to a file using `plt.savefig` or `fig.savefig`).
`plt.show` will show the image to you and will block the script to allow you to take in and adjust the figure before saving or discarding it.

<a class="anchor" id="backends"></a>
### Changing where the image appears: backends
Matplotlib works across a wide range of environments: Linux, Mac OS, Windows, in the browser, and more. 
The exact detail of how to show you your plot will be different across all of these environments.
This procedure used to translate your `Figure`/`Axes` objects into an actual visualisation is called the backend.

In this notebook we were using the `inline` backend, which is the default when running in a notebook.
While very robust, this backend has the disadvantage that it only produces static plots.
We could have had interactive plots if only we had changed backends to `nbagg`.
You can change backends in the IPython terminal/notebook using:
```
%matplotlib nbagg
```
> If you are using Jupyterlab (new version of the jupyter notebook) the `nbagg` backend will not work. Instead you will have to install `ipympl` and then use the `widgets` backend to get an interactive backend (this also works in the old notebooks).

In python scripts, this will give you a syntax error and you should instead use:
```
import matplotlib
matplotlib.use("MacOSX")
```
Usually, the default backend will be fine, so you will not have to set it. 
Note that setting it explicitly will make your script less portable.